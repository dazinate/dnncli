﻿using Dazinate.Dnn.Cli;
using System;
using System.IO;

namespace Dnn.Cli.Samples.Console
{
    class Program
    {

        static void Main(string[] args)
        {
            // Change this to point to the dnn website folder.
            string dnnWebsitePath = "C:\\inetpub\\wwwroot\\dnn9\\";

            // Uncomment this if you have used the GAC strategy and wish to uninstall from the GAC.
            // var result = GacHelper.EnsureRemovedFromGac("Dazinate.Dnn.Cli");

            using (var copyToBinFolder = new CopyToWebsiteBinFolderStrategy(dnnWebsitePath))
            {
                using (var dnnCommandClient = new CommandClient(copyToBinFolder, dnnWebsitePath))
                {
                    var installPackageCommand = dnnCommandClient.CreateCommand<DnnInstallPackageCommand>();
                    var zipFilePath = Path.Combine(Environment.CurrentDirectory, "DnnExtension136_0.0.0_Install.zip");
                    installPackageCommand.PackageFilePath = zipFilePath;

                    var success = dnnCommandClient.ExecuteCommand(installPackageCommand);
                    if (!success)
                    {
                        System.Console.WriteLine("failed to install package.");
                        foreach (var item in installPackageCommand.LogOutput)
                        {
                            if (item.Key == "Failure")
                            {
                                System.Console.WriteLine(item.Key + ": " + item.Value);
                            }
                        }
                    }
                }
                // _Installer.in
                System.Console.WriteLine("Finished");

            }
        }

        //public static string AssemblyDirectory
        //{
        //    get
        //    {
        //        string codeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
        //        UriBuilder uri = new UriBuilder(codeBase);
        //        string path = Uri.UnescapeDataString(uri.Path);
        //        return Path.GetDirectoryName(path);
        //    }
        //}
    }




}







