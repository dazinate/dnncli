﻿namespace Dazinate.Dnn.Cli
{

    public interface ICommandHost
    {

        string GetVersionInfo();

        string DnnWebsitePath { get; }
    }
}