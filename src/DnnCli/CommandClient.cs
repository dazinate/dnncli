﻿using System;
using System.IO;
using System.Reflection;

namespace Dazinate.Dnn.Cli
{
    public class CommandClient : IDisposable
    {
        private Lazy<AppDomain> _appDomain;
        private Lazy<dynamic> _host;
        private IExecutionStrategy _executionStrategy;

        public DnnCommandHost CommandHost { get { return _host.Value; } }

        public CommandClient(IExecutionStrategy executionStrategy, string dnnWebsitePath)
        {
            _executionStrategy = executionStrategy;
            _appDomain = new Lazy<AppDomain>(() =>
            {
                // Create an app domain, to load Dnn in, with proxy.
                var webConfigPath = Path.Combine(dnnWebsitePath, "web.config");
                var appDomainSetup = new AppDomainSetup();
                appDomainSetup.ApplicationBase = dnnWebsitePath;
                appDomainSetup.ConfigurationFile = webConfigPath;
                appDomainSetup.ApplicationName = "Dnn9";
                appDomainSetup.PrivateBinPathProbe = appDomainSetup.PrivateBinPath;

                var appDomainName = Guid.NewGuid();
                var appDomain = AppDomain.CreateDomain(appDomainName.ToString(), null, appDomainSetup);
                return appDomain;
            });

            _host = new Lazy<dynamic>(() =>
            {
                var proxyType = typeof(DnnCommandHost);
                dynamic proxyDynamic = _appDomain.Value
                    .CreateInstanceAndUnwrap(proxyType.Assembly.FullName
                    , typeof(DnnCommandHost).FullName);
                return proxyDynamic;
            });

            var additionalProbingPath = AssemblyDirectory;
            var probingPaths = new string[] { additionalProbingPath };

            try
            {
                // DnnCommandHost proxy = (DnnCommandHost)proxyDynamic;
                // Call the bootstrap method on the proxy, this starts the Dnn web application which bootstraps dnn.
               
                var proxy = _host.Value;
                proxy.Bootstrap(dnnWebsitePath, probingPaths);

            }
            catch (Exception e)
            {
                string hostCodebase = _host.Value.GetType().Assembly.CodeBase;
                var expectedCodeBase = typeof(DnnCommandHost).Assembly.CodeBase;
                throw new Exception("Unable to cast instance of host from assembly: " + hostCodebase + " to instance of type in referenced assembly: " + expectedCodeBase, e);

            }



        }

        public TCommand CreateCommand<TCommand>()
            where TCommand : DnnCommand
        {
            // Todo - the command must be resolvable by the host, we let the execution strategy decide
            // how that happens (i.e could either defer to Gac, or dnn website bin folder or other location.
            //   _executionStrategy.EnsureCommandAssembly<TCommand>(_host);

            var assy = typeof(TCommand).Assembly;
            var assyName = assy.GetName();
            var location = GetAssemblyPath(assy);
            var directory = Path.GetDirectoryName(location);
            _host.Value.EnsureProbingPath(directory);
            _host.Value.EnsureAssemblyLoaded(assyName);
            var proxyType = typeof(TCommand);
            TCommand proxy = (TCommand)_appDomain.Value.CreateInstanceAndUnwrap(proxyType.Assembly.FullName, typeof(TCommand).FullName);
            return proxy;
        }

        public static string GetAssemblyPath(Assembly assy)
        {
            string codeBase = assy.CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            return path.Replace('/', '\\');
        }

        public bool ExecuteCommand<TCommand>(TCommand command)
          where TCommand : DnnCommand
        {
            return CommandHost.ExecuteCommand(command);
        }

        public string GetVersionInfo()
        {
            return ".NET Version: " + Environment.Version.ToString() + "\r\n" +
            "wwReflection Assembly: " + typeof(CommandClient).Assembly.CodeBase.Replace("file:///", "").Replace("/", "\\") + "\r\n" +
            "Assembly Cur Dir: " + Directory.GetCurrentDirectory() + "\r\n" +
            "ApplicationBase: " + AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "\r\n" +
            "App Domain: " + AppDomain.CurrentDomain.FriendlyName + "\r\n";
        }

        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    // in the end, unload the domain
                    if(_appDomain.IsValueCreated)
                    {
                        AppDomain.Unload(_appDomain.Value);
                    }
                  
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~CommandClient() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }

}
