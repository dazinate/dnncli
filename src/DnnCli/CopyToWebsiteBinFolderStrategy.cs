﻿using System;
using System.IO;
using System.Reflection;

namespace Dazinate.Dnn.Cli
{
    public class CopyToWebsiteBinFolderStrategy : IExecutionStrategy
    {

        private string _websiteBinDirectory;

        public CopyToWebsiteBinFolderStrategy(string dnnWebsitePath)
        {
            _websiteBinDirectory = Path.Combine(dnnWebsitePath, "bin");
            var proxyType = typeof(DnnCommandHost).Assembly;
            // Copy the proxy assembly to the website bin dir.
            CopyAssemblyToWebsiteBinDirectory(proxyType);
        }

        //public void EnsureBootstrapAssembly<TDnnCommand>() where TDnnCommand : DnnCommand
        //{
        //    var assy = typeof(TDnnCommand).Assembly;
        //    CopyAssemblyToWebsiteBinDirectory(assy);
        //}

        public static string AssemblyPath
        {
            get
            {
                return GetAssemblyPath(Assembly.GetExecutingAssembly());
            }
        }

        private void CopyAssemblyToWebsiteBinDirectory(Assembly assy)
        {
            var path = GetAssemblyPath(assy);
            var assyName = assy.GetName().Name + ".dll";

            // var proxyAssyPath = Path.Combine(AssemblyDirectory, assyName);
            var targetPath = Path.Combine(_websiteBinDirectory, assyName);

            if (!File.Exists(targetPath))
            {
                File.Copy(path, targetPath);
            }
        }

        public static string GetAssemblyPath(Assembly assy)
        {
            string codeBase = assy.CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            return path.Replace('/', '\\');
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                  //  GacHelper.RemoveFromGac(AssemblyPath);
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~GacStrategy() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
               
        #endregion
    }
}