﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Dazinate.Dnn.Cli
{

    public class DnnCommandHost : MarshalByRefObject, ICommandHost
    {

        private static string _DnnWebsitePath; //= "C:\\inetpub\\wwwroot\\dnn9\\";
        private static string _DnnWebsiteWebConfigPath;
        private static List<string> _ProbingPaths;

        public string DnnWebsitePath
        {
            get
            {
                return _DnnWebsitePath;
            }
        }

        public DnnCommandHost()
        {


        }

        public void EnsureProbingPath(string path)
        {
            if (!_ProbingPaths.Contains(path))
            {
                //todo share lock with CurrentDomain_AssemblyResolve before modifying.
                _ProbingPaths.Add(path);
            }
        }

        public void Bootstrap(string websitePath, string[] assemblyProbingPaths)
        {
            _DnnWebsitePath = websitePath;
            _ProbingPaths = assemblyProbingPaths.ToList();

            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
            _DnnWebsiteWebConfigPath = _DnnWebsitePath + "web.config";

            AppDomain.CurrentDomain.SetData("APP_CONFIG_FILE", _DnnWebsiteWebConfigPath);
            AppDomain.CurrentDomain.SetData("WEB_CONFIG_FILE", _DnnWebsiteWebConfigPath);

            var dnnWebAssemblyPath = Path.Combine(websitePath, "bin\\dotnetnuke.web.dll");

            var assy = Assembly.LoadFile(dnnWebAssemblyPath);
            var name = assy.GetName();
            var version = name.Version;

            if (version.Major < 7)
            {
                throw new NotSupportedException("Only Dnn version 7 and higher supported.");
            }

            var typeName = "DotNetNuke.Web.Common.Internal.DotNetNukeHttpApplication";
            var httpAppType = assy.GetType(typeName);
            var instance = Activator.CreateInstance(httpAppType, true);

            var appStart = httpAppType.GetMethod("Application_Start", BindingFlags.Instance | BindingFlags.NonPublic);
            object sender = null;
            var args = EventArgs.Empty;

            var instanceArgs = new object[] { sender, args };
            appStart.Invoke(instance, instanceArgs);


        }

        internal void EnsureAssemblyLoaded(AssemblyName assyName)
        {
            // load assy into domain.
            var assy = Assembly.Load(assyName);
        }

        public bool ExecuteCommand<TCommand>(TCommand command) where TCommand : DnnCommand
        {
            try
            {
                using (new FakeHttpContext.FakeHttpContext())
                {
                    return command.Execute(this);
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.ToString());
                return false;
            }

        }

        public string GetVersionInfo()
        {
            return ".NET Version: " + Environment.Version.ToString() + "\r\n" +
            "wwReflection Assembly: " + typeof(DnnCommandHost).Assembly.CodeBase.Replace("file:///", "").Replace("/", "\\") + "\r\n" +
            "Assembly Cur Dir: " + Directory.GetCurrentDirectory() + "\r\n" +
            "ApplicationBase: " + AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "\r\n" +
            "App Domain: " + AppDomain.CurrentDomain.FriendlyName + "\r\n";
        }

        private Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            //todo: chare lock with add probing path();
            var probingPaths = _ProbingPaths.ToArray();
            Console.WriteLine("looking for: " + args.Name);

            if (args.RequestingAssembly != null)
            {
                Console.WriteLine("required by: " + args.RequestingAssembly);
            }

            var assyName = new AssemblyName(args.Name);

            foreach (var probingPath in probingPaths)
            {
                var newPath = Path.Combine(probingPath, assyName.Name);
                var dllPath = newPath + ".dll";
                var exePath = newPath + ".exe";

                //if (!newPath.EndsWith(".dll"))
                //{
                //    newPath = newPath + ".dll";
                //}
                if (File.Exists(dllPath))
                {
                    var assy = System.Reflection.Assembly.LoadFile(dllPath);
                    return assy;
                }
                else if (File.Exists(exePath))
                {
                    var assy = System.Reflection.Assembly.LoadFile(exePath);
                    return assy;
                }
            }

            //try
            //{
            //    var assy = Assembly.Load(assyName);
            //    return assy;
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.ToString());
            //    // throw;
            //}

            Console.WriteLine("Unable to find: " + args.Name);

            return null;

            // throw new DllNotFoundException(args.Name);
        }
    }
}
