﻿using System;
using System.Reflection;

namespace Dazinate.Dnn.Cli
{

    public class GacExecutionStrategy : IExecutionStrategy
    {

        public GacExecutionStrategy()
        {
            var dllPath = AssemblyPath;
            GacHelper.AddToGac(dllPath);
        }

        //public void EnsureBootstrapAssembly<TDnnCommand>() 
        //    where TDnnCommand : DnnCommand
        //{
        //    var assy = typeof(TDnnCommand).Assembly;
        //    var assyPath = GetAssemblyPath(assy);
        //    GacHelper.AddToGac(assyPath);
        //}

        public static string AssemblyPath
        {
            get
            {
                return GetAssemblyPath(Assembly.GetExecutingAssembly());                
            }
        }

        public static string GetAssemblyPath(Assembly assy)
        {
            string codeBase = assy.CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            return path.Replace('/', '\\');
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    GacHelper.RemoveFromGac(AssemblyPath);
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~GacStrategy() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }


        #endregion
    }
}