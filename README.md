## Dnn Cli

This library provides a mechanism for creating and executing commands that run as if they are operating within a Dnn Website application, allowing you to leverage the Dnn Api, even from outside of a Dnn website application. 

## How it works

The Dnn Cli, creates its own special `AppDomain`, which it bootstraps a copy of the target Dnn website instance into.
This `AppDomain` has a copy of the target nn website instance running within it. It doesn't interfere with the actual website.

When a command is executed, the Dnn Cli first marshall's the command into this special Dnn AppDomain.
It then executes the command, within the context of a fake `HttpRequest` which it generates for the lifetime of that command.
This allows the comand to access the current `HttpContext` and use all of the normal Dnn Api's as if it was being executed within the actual website.
Once the command has finished executing, the command is returned accross the AppDomain boundary, back to the host `AppDomain` / your domain, where you can inspect the results of that command.

## Getting started

1 Create a new `csharp` command line project, and add the `Dazinate.Dnn.Cli` nuget package.

2 Implement a `Command` by inheriting from `DnnCommand`.

For example, implement a command that can install an extension zip package to a target Dnn website:

```csharp
   public class DnnInstallPackageCommand : DnnCommand
    {

        public DnnInstallPackageCommand()
        {


        }

        public string PackageFilePath { get; set; }

        public List<KeyValuePair<string, string>> LogOutput { get; set; }

        public override bool Execute(ICommandHost host)
        {
            // You can use all the standard DNN Api's here - stuff works as if you are running inside of the dnn website.
            var appPath = HttpContext.Current.Request.ApplicationPath;
            var installPath = appPath + "Install/";

            var mappedPath = HttpContext.Current.Server.MapPath(installPath);

            var stream = File.OpenRead(this.PackageFilePath);
  
            var installer = new DotNetNuke.Services.Installer.Installer(stream, host.DnnWebsitePath, true);
                   
            this.LogOutput = new List<KeyValuePair<string, string>>();

            try
            {
                bool valid = installer.IsValid;
                var installerInfo = installer.InstallerInfo;
                var logger = installerInfo.Log;
                var logs = logger.Logs;
                bool failure = false;

                foreach (var item in logs)
                {
                    if (item.Type.ToString() == "Failure")
                    {
                        failure = true;
                    }
                    this.LogOutput.Add(new KeyValuePair<string, string>(item.Type.ToString(), item.Description));
                }

                if (valid)
                {
                    installer.Install();
                }

                return valid && !failure;              

            }
            catch (Exception e)
            {

                throw;
            }

        }


    }

```

You can now execute this command, within the context of a target Dnn website - you just need to know the physical path of the dnn website.
In your main entry point in `Program.cs` put this:

```
          
			var dnnWebsitePath = "C:\inetpub\wwwroot\dnn9"; // path to physical website directory.

			// The strategy determines how specially created Dnn AppDomain will resolve the Dnn.Cli assembly.
            using (var strategy = new CopyToWebsiteBinFolderStrategy(dnnWebsitePath)) // Dnn.Cli.dll will be copied to the websites "bin" folder. alternative is to use Gac strategy but that requires GAC permissions.
            {
                using (var dnnCommandClient = new CommandClient(strategy, dnnWebsitePath))
                {
				    // Creates the command in the Dnn AppDomain and returns a proxy to it.
                    var installPackageCommand = dnnCommandClient.CreateCommand<DnnInstallPackageCommand>();
					
					// Populate command arguments. In this case, specificy the path to the install zip.
                    var zipFilePath = Path.Combine(Environment.CurrentDirectory, "DnnExtension136_0.0.0_Install.zip");
                    installPackageCommand.PackageFilePath = zipFilePath;

					// Execute the command and get back if it executed successfully. 
					// It will execute within the Dnn AppDomain.
                    var success = dnnCommandClient.ExecuteCommand(installPackageCommand);
                    if (!success)
                    {
					    // The command populates its Logs property, so we can see why the install failed.
                        Console.WriteLine("failed to install package.");
                        foreach (var item in installPackageCommand.LogOutput)
                        {
                            if (item.Key == "Failure")
                            {
                                Console.WriteLine(item.Key + ": " + item.Value);
                            }
                        }
                    }
                }              

            }


```